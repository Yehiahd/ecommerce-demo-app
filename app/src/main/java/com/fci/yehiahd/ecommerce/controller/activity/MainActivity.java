package com.fci.yehiahd.ecommerce.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.FrameLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.api.RxFacebook;
import com.fci.yehiahd.ecommerce.database.SharedPreferenceHelper;
import com.fci.yehiahd.ecommerce.util.Constant;
import com.fci.yehiahd.ecommerce.util.JsonParser;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.facebook_login_button)
    LoginButton mFacebookLoginButton;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.frame)
    FrameLayout frame;

    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isSessionActive()) {
            startActivity(new Intent(MainActivity.this, FeedActivity.class));
            this.finish();
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUi();
        initFacebookLogin();

    }

    private void initUi() {
        setSupportActionBar(toolbar);
    }

    private void initFacebookLogin() {
        mCallbackManager = CallbackManager.Factory.create();
        mFacebookLoginButton.setReadPermissions(Collections.singletonList(Constant.FacebookPermissions.publicProfilePermission));
        RxFacebook.observeRegister(mCallbackManager, mFacebookLoginButton)
                .subscribe(loginResult -> userGranted(loginResult.getAccessToken()),
                        throwable -> Log.d("Failed", throwable.getMessage()));
    }

    private void userGranted(AccessToken token) {
        RxFacebook.getUserJson(token)
                .flatMap(JsonParser::getUserFromJson)
                .compose(bindToLifecycle())
                .subscribe(facebookUser -> {
                            if (SharedPreferenceHelper.getInstance().saveUser(this, facebookUser, true)) {
                                startActivity(new Intent(MainActivity.this, FeedActivity.class));
                                this.finish();
                            }

                        },
                        throwable -> Log.d("Error Occurred", throwable.getMessage()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
