package com.fci.yehiahd.ecommerce.controller.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.database.DatabaseHelper;
import com.fci.yehiahd.ecommerce.model.FeedItem;
import com.fci.yehiahd.ecommerce.util.Constant;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AddItemActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.item_image_view)
    ImageView itemImageView;
    @BindView(R.id.camera_ic_holder_FL)
    FrameLayout cameraIcHolderFL;
    @BindView(R.id.item_img_card)
    CardView itemImgCard;
    @BindView(R.id.item_title_ET)
    EditText itemTitleET;
    @BindView(R.id.item_description_ET)
    EditText itemDescriptionET;
    @BindView(R.id.add_item_btn)
    Button addItemBtn;
    @BindView(R.id.root_view_add_item)
    ScrollView rootViewAddItem;
    @BindView(R.id.progress_bar_add_item)
    ProgressBar progressBarAddItem;

    private String imgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        ButterKnife.bind(this);
        setupUi();
    }


    private void setupUi() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addItemBtn.setOnClickListener(this);
        itemImgCard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_item_btn:
                addNewItem();
                break;

            case R.id.item_img_card:
                showPhotoSelectionOptionSheet();
                break;
        }
    }

    private void addNewItem() {

        if (this.imgUrl == null){
            infoToasty(getString(R.string.add_image));
        }

        else if (isDataValid(itemTitleET, itemDescriptionET)) {
            String title = itemTitleET.getText().toString();
            String description = itemDescriptionET.getText().toString();
            String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
            FeedItem item = new FeedItem(title,timeStamp,description,imgUrl,Constant.Utils.NOT_FAVORITE);
            enableDisableRoot(rootViewAddItem, !Constant.Utils.ENABLE);
            showView(progressBarAddItem);
            if (DatabaseHelper.getInstance(this).addItem(getCurrentUser().getId(),item)){
                clearFields(itemTitleET,itemDescriptionET);
                this.finish();
            }
        }
    }

    private void showPhotoSelectionOptionSheet() {
        new BottomSheet.Builder(this)
                .setTitle(Constant.Utils.BOTTOM_SHEET_TITLE)
                .setSheet(R.menu.bottom_sheet)
                .setListener(new BottomSheetListener() {
                    @Override
                    public void onSheetShown(@NonNull BottomSheet bottomSheet) {

                    }

                    @Override
                    public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.choose_from_camera:
                                takePhotoFromCamera();
                                break;
                            case R.id.choose_from_documents:
                                takePhotoFromGalary();
                                break;
                        }
                    }

                    @Override
                    public void onSheetDismissed(@NonNull BottomSheet bottomSheet, @DismissEvent int i) {

                    }
                }).show();
    }

    private void takePhotoFromGalary() {
        RxPaparazzo.takeImage(this)
                .crop()
                .usingGallery()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.resultCode() != RESULT_OK) {
                        return;
                    }

                    response.targetUI().loadImage(response.data());
                });
    }

    private void takePhotoFromCamera() {
        RxPaparazzo.takeImage(this)
                .crop()
                .usingCamera()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response.resultCode() != RESULT_OK) {
                        return;
                    }

                    response.targetUI().loadImage(response.data());
                });
    }

    private void loadImage(String filePath) {
        hideView(cameraIcHolderFL);
        this.imgUrl = Constant.Utils.BASE_INTERNAL_URL + filePath;
        Picasso.with(this)
                .load(imgUrl)
                .placeholder(R.drawable.progress_placeholder)
                .into(itemImageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
