package com.fci.yehiahd.ecommerce.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yehia on 04/03/17.
 */

public class FacebookUser implements Parcelable {

    private String id;
    private String userName;
    private String imgUrl;

    public FacebookUser(String id, String userName, String imgUrl) {
        this.id = id;
        this.userName = userName;
        this.imgUrl = imgUrl;
    }

    private FacebookUser(Builder builder) {
        setUserName(builder.userName);
        setImgUrl(builder.imgUrl);
        setId(builder.id);
    }

    public static Builder buildFacebookUser() {
        return new Builder();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static final class Builder {
        private String userName;
        private String imgUrl;
        private String id;

        private Builder() {
        }

        public Builder setUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
            return this;
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public FacebookUser build() {
            return new FacebookUser(this);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.userName);
        dest.writeString(this.imgUrl);
    }

    protected FacebookUser(Parcel in) {
        this.id = in.readString();
        this.userName = in.readString();
        this.imgUrl = in.readString();
    }

    public static final Parcelable.Creator<FacebookUser> CREATOR = new Parcelable.Creator<FacebookUser>() {
        @Override
        public FacebookUser createFromParcel(Parcel source) {
            return new FacebookUser(source);
        }

        @Override
        public FacebookUser[] newArray(int size) {
            return new FacebookUser[size];
        }
    };
}
