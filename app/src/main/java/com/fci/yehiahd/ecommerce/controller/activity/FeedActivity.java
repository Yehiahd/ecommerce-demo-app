package com.fci.yehiahd.ecommerce.controller.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.adapter.ViewPagerAdapter;
import com.fci.yehiahd.ecommerce.controller.fragment.AllFeedsFragment;
import com.fci.yehiahd.ecommerce.controller.fragment.FavoriteFragment;
import com.fci.yehiahd.ecommerce.controller.fragment.MyFeedsFragment;
import com.fci.yehiahd.ecommerce.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedActivity extends BaseActivity {

    @BindView(R.id.toolbar_feed)
    Toolbar toolbarFeed;
    @BindView(R.id.tabs_feed)
    TabLayout tabLayout;
    @BindView(R.id.viewpager_feed)
    ViewPager viewpagerFeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        ButterKnife.bind(this);
        initUi();
    }

    private void initUi() {
        toolbarFeed = (Toolbar) findViewById(R.id.toolbar_feed);
        setSupportActionBar(toolbarFeed);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewpagerFeed = (ViewPager) findViewById(R.id.viewpager_feed);
        setupViewPager(viewpagerFeed);

        tabLayout = (TabLayout) findViewById(R.id.tabs_feed);
        tabLayout.setupWithViewPager(viewpagerFeed);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllFeedsFragment(), Constant.Fragments.FEEDS);
        adapter.addFragment(new MyFeedsFragment(), Constant.Fragments.MY_ITEMS);
        adapter.addFragment(new FavoriteFragment(), Constant.Fragments.FAVORITES);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
