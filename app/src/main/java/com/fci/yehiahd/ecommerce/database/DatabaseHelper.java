package com.fci.yehiahd.ecommerce.database;

import android.content.Context;

/**
 * Created by yehia on 04/03/17.
 */

public class DatabaseHelper {

    private static DBConnection mDbConnection;

    public static DBConnection getInstance(Context mContext){

        if (mDbConnection == null){
            mDbConnection = new DBConnection(mContext);
        }

        return mDbConnection;
    }
}
