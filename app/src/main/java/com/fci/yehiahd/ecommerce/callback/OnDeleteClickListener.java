package com.fci.yehiahd.ecommerce.callback;

/**
 * Created by yehia on 05/03/17.
 */

public interface OnDeleteClickListener {
    void onDeleted();
}
