package com.fci.yehiahd.ecommerce.controller.fragment;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.database.SharedPreferenceHelper;
import com.fci.yehiahd.ecommerce.model.FacebookUser;
import com.fci.yehiahd.ecommerce.util.Connection;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    public BaseFragment() {
        // Required empty public constructor
    }

    protected boolean isConnected(){
        return Connection.isNetworkAvailable(getActivity());
    }

    protected void showView(View view){
        view.setVisibility(View.VISIBLE);
    }

    protected void hideView(View view){
        view.setVisibility(View.GONE);
    }


    protected FacebookUser getDummyUser() {
        String imgUrl = "https://firebasestorage.googleapis.com/v0/b/tawsela-9a5a0.appspot.com/o/users%2FfktTETgjqsXSxes4PEsENvHvYo63%2FIMG-08022017_191211_473.jpg?alt=media&token=43c812d3-d2d9-49ef-a9b3-ccf926308664";
        return new FacebookUser(getString(R.string.dummy_user_id),getString(R.string.dummy_user_name),imgUrl);
    }

    protected FacebookUser getCurrentUser(Context context){
        return SharedPreferenceHelper.getInstance().getUser(context);
    }

}
