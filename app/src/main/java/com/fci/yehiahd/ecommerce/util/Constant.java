package com.fci.yehiahd.ecommerce.util;

/**
 * Created by yehia on 04/03/17.
 */

public interface Constant {

    interface FacebookPermissions {
        String publicProfilePermission = "public_profile";
    }

    interface Graph {
        String PICTURE = "picture";
        String DATA = "data";
        String URL = "url";
        String ID = "id";
        String NAME = "name";
        String FIELDS = "fields";
        String FIELDS_ATTRIBUTES = "picture.type(large),name,id";
    }

    interface Fragments {
        String FEEDS = "feeds";
        String MY_ITEMS = "my items";
        String FAVORITES = "favorites";
    }

    interface Utils {
        String BOTTOM_SHEET_TITLE = "Choose an action";
        boolean ENABLE = true;
        boolean NOT_FAVORITE = false;
        boolean FAVORITE = true;
        String ALERT_DIALOG_TITLE = "Delete";
        String ALERT_DIALOG_MESSAGE = "Are you sure delete this item ?";
        String ALERT_DIALOG_POSITIVE_BTN = "Delete";
        String ALERT_DIALOG_NEGATIVE_BTN = "Cancel";
        String BASE_INTERNAL_URL = "file://";
    }

    interface Database {
        int FAVORITE = 1;
        int NOT_FAVORITE = 0;

    }

    interface SharedPreference {
        String USER_TAG = "user";
    }

}
