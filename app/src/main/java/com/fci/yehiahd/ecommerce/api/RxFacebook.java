package com.fci.yehiahd.ecommerce.api;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fci.yehiahd.ecommerce.util.Constant;

import org.json.JSONObject;

import rx.Emitter;
import rx.Observable;

/**
 * Created by yehia on 04/03/17.
 */

public class RxFacebook {

    public static Observable<LoginResult> observeRegister(CallbackManager callbackManager, LoginButton loginButton){
        return Observable.fromEmitter(loginResultEmitter ->
                loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                loginResultEmitter.onNext(loginResult);
                loginResultEmitter.onCompleted();
            }

            @Override
            public void onCancel() {
                loginResultEmitter.onError(new Throwable("Canceled"));
            }

            @Override
            public void onError(FacebookException error) {
                loginResultEmitter.onError(error);
            }
        }), Emitter.BackpressureMode.NONE);
    }

    public static Observable<JSONObject> getUserJson(AccessToken token){
        return Observable.fromEmitter(emitter -> {
            Bundle graphRequestInfo = new Bundle();
            graphRequestInfo.putString(Constant.Graph.FIELDS, Constant.Graph.FIELDS_ATTRIBUTES);
            GraphRequest graphRequest = GraphRequest.newMeRequest(token, (object, response) -> {
                JSONObject userData = response.getJSONObject();
                try {
                    emitter.onNext(userData);
                    emitter.onCompleted();
                }
                catch (Exception e){
                    emitter.onError(e);
                }
            });
            graphRequest.setParameters(graphRequestInfo);
            graphRequest.executeAsync();
        }, Emitter.BackpressureMode.NONE);
    }

}
