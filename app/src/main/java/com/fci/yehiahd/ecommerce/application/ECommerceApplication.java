package com.fci.yehiahd.ecommerce.application;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.miguelbcr.ui.rx_paparazzo.RxPaparazzo;

public class ECommerceApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        RxPaparazzo.register(this);
    }

}
