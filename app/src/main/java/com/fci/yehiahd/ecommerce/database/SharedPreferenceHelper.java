package com.fci.yehiahd.ecommerce.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fci.yehiahd.ecommerce.model.FacebookUser;
import com.fci.yehiahd.ecommerce.util.Constant;
import com.google.gson.Gson;

/**
 * Created by yehia on 04/03/17.
 */

public class SharedPreferenceHelper {

    private static SharedPreferenceHelper mSharedPreferenceHelper;
    private FacebookUser mUser;

    private SharedPreferenceHelper(){

    }

    public static SharedPreferenceHelper getInstance(){
        if (mSharedPreferenceHelper == null) {
            synchronized (SharedPreferenceHelper.class) {
                if (mSharedPreferenceHelper == null) {
                    mSharedPreferenceHelper = new SharedPreferenceHelper();
                }
            }
        }
        return mSharedPreferenceHelper;
    }

    public FacebookUser getUser(Context context) {
        if (mUser == null) {
            synchronized (FacebookUser.class) {
                if (mUser == null) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                    String userString = sharedPreferences.getString(Constant.SharedPreference.USER_TAG, null);
                    if (userString != null) {
                        mUser = null;
                        mUser = new Gson().fromJson(userString, FacebookUser.class);

                        }

                        else
                            return null;
                    }
                }
            }

        return mUser;
    }

    public synchronized boolean saveUser(Context context, FacebookUser user, boolean uiThread) {
        if (user == null) {
            return false;
        }
        String userString = new Gson().toJson(user);
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(Constant.SharedPreference.USER_TAG, userString);
        if (uiThread) {
            editor.apply();
            return true;
        } else {
            if (editor.commit()) {
                mUser = user;
                return true;
            } else {
                return false;
            }
        }
    }
}
