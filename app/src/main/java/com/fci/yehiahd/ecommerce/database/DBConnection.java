package com.fci.yehiahd.ecommerce.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fci.yehiahd.ecommerce.model.FeedItem;

import java.util.ArrayList;

/**
 * Created by yehia on 04/03/17.
 */

public class DBConnection extends SQLiteOpenHelper {

    //Database Name
    private static final String DB_NAME = "eCommerce.db";

    //Database Version
    private static final int DB_VERSION = 3;

    //Table Names
    private static final String TABLE_ITEM = "item";
//    private static final String TABLE_USER = "user";

    //Common Column Names
    private static final String USER_ID = "user_id";

    //Item table Column Names
    private static final String TITLE = "title";
    private static final String TIME_STAMP = "time_stamp";
    private static final String DESCRIPTION = "description";
    private static final String ITEM_IMG_URL = "item_img_url";
    private static final String IS_FAVORITE = "is_favorite";
    private static final String ITEM_ID = "item_id";


    //User table Column Names
//    private static final String NAME = "name";
//    private static final String USER_IMG_URL = "user_img_url";

    //Item table create statement
    private static final String CREATE_ITEM_TABLE = "CREATE TABLE "
            + TABLE_ITEM + "("
            + USER_ID + " TEXT,"
            + TITLE + " TEXT,"
            + TIME_STAMP + " TEXT,"
            + DESCRIPTION + " TEXT,"
            + ITEM_IMG_URL + " TEXT,"
            + IS_FAVORITE + " INTEGER DEFAULT 0,"
            + ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
            + ")";

//    private static final String CREATE_USER_TABLE = "CREATE TABLE "
//            + TABLE_USER + "("
//            + USER_ID + " TEXT PRIMARY KEY,"
//            + NAME + " TEXT,"
//            + USER_IMG_URL + " TEXT"
//            + ")";

    private static final String DROP_TABLE_STATEMENT = "DROP TABLE IF EXISTS ";

    private static int NOT_FAVORITE = 0;
    private static int FAVORITE = 1;

    public DBConnection(Context context){
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ITEM_TABLE);
//        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_STATEMENT + TABLE_ITEM);
//        db.execSQL(DROP_TABLE_STATEMENT + TABLE_USER);

        onCreate(db);
    }

//    public boolean addUser(FacebookUser user){
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//
//        values.put(USER_ID,user.getId());
//        values.put(NAME ,user.getUserName());
//        values.put(USER_IMG_URL,user.getImgUrl());
//
//        long rowId = db.insert(TABLE_USER,null,values);
//
//        return rowId != -1;
//    }

    public boolean addItem(String userId, FeedItem item){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(USER_ID,userId);
        values.put(TITLE,item.getTitle());
        values.put(TIME_STAMP,item.getTimeStamp());
        values.put(DESCRIPTION,item.getDescription());
        values.put(ITEM_IMG_URL,item.getImgUrl());
        values.put(IS_FAVORITE,NOT_FAVORITE);
//        values.put(ITEM_ID,item.getId());

        long rowId = db.insert(TABLE_ITEM,null,values);
        return rowId != -1;
    }

    public ArrayList<FeedItem> getAllItems(){
        String query = "SELECT * FROM "+TABLE_ITEM;
        return getItems(query);
    }

    public ArrayList<FeedItem> getMyItems(String userId){
        String query = "SELECT * FROM "+TABLE_ITEM+" WHERE "+USER_ID+"="+userId;
        return getItems(query);
    }

    public ArrayList<FeedItem> getMyFavoriteItems(String userId){
//        String query = "SELECT * FROM "+TABLE_ITEM+" WHERE "+USER_ID+"="+userId + " AND " + IS_FAVORITE + "=" + FAVORITE;
        String query = "SELECT * FROM "+TABLE_ITEM+" WHERE "+IS_FAVORITE + "=" + FAVORITE;
        return getItems(query);
    }

    public int getItemCount(){
        String countQuery = "SELECT * FROM "+TABLE_ITEM ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public void deleteItem(int itemId){
        String query = "DELETE FROM "+TABLE_ITEM+" WHERE "+ITEM_ID+"="+itemId;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void setFavorite(int itemId,int isFavorite){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        if (isFavorite == FAVORITE){
            values.put(IS_FAVORITE,FAVORITE);
        }
        else {
            values.put(IS_FAVORITE,NOT_FAVORITE);
        }

        db.update(TABLE_ITEM,values,ITEM_ID+"="+itemId,null);
    }

    public ArrayList<FeedItem> getItems(String query){
        int i=0,itemCount = getItemCount();
        ArrayList<FeedItem> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery(query,null);
        res.moveToFirst();

        while (!res.isAfterLast()){
            FeedItem item = new FeedItem();
            item.setId(res.getInt(res.getColumnIndex(ITEM_ID)));
            item.setTitle(res.getString(res.getColumnIndex(TITLE)));
            item.setDescription(res.getString(res.getColumnIndex(DESCRIPTION)));
            item.setImgUrl(res.getString(res.getColumnIndex(ITEM_IMG_URL)));
            item.setTimeStamp(res.getString(res.getColumnIndex(TIME_STAMP)));

            int isFavorite = res.getInt(res.getColumnIndex(IS_FAVORITE));

            if (isFavorite == NOT_FAVORITE)
                item.setFavorite(false);
            else
                item.setFavorite(true);

            if (i == itemCount)
                break;
            list.add(item);
            res.moveToNext();
            i++;
        }
        res.close();
        return list;
    }

}
