package com.fci.yehiahd.ecommerce.controller.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.adapter.FeedsAdapter;
import com.fci.yehiahd.ecommerce.database.DatabaseHelper;
import com.fci.yehiahd.ecommerce.model.FacebookUser;
import com.fci.yehiahd.ecommerce.model.FeedItem;
import com.fci.yehiahd.ecommerce.util.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFeedsFragment extends BaseFragment {


    @BindView(R.id.recycler_my_feeds)
    RecyclerView recyclerMyFeeds;
    @BindView(R.id.no_items_founded)
    TextView noItemsFounded;

    private FeedsAdapter mFeedsAdapter;
    private List<FeedItem> feedItemList;
    private FacebookUser mUser;
    private RecyclerView.LayoutManager mLayoutManager;

    public MyFeedsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_feeds, container, false);
        ButterKnife.bind(this, view);
        initUi();
        setupUi();
        setListeners();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (feedItemList.size() != getMyFeeds().size()){
            feedItemList = getMyFeeds();
            mFeedsAdapter.updateFeedsList(feedItemList);
            updateUi();
        }
    }

    private void initUi() {
        mUser = getCurrentUser(getActivity());
        feedItemList = getMyFeeds();
        mFeedsAdapter = new FeedsAdapter(getActivity(), feedItemList, mUser, Constant.Utils.ENABLE);
        mLayoutManager = new LinearLayoutManager(getActivity());
    }

    private void setupUi() {
        recyclerMyFeeds.setLayoutManager(mLayoutManager);
        recyclerMyFeeds.setItemAnimator(new DefaultItemAnimator());
        recyclerMyFeeds.setAdapter(mFeedsAdapter);
        if (feedItemList.size() == 0)
            showView(noItemsFounded);
    }

    private void setListeners() {
        mFeedsAdapter.addFavoriteListener(this::performActionOnListen);
        mFeedsAdapter.addDeleteListener(this::performActionOnListen);
    }

    private List<FeedItem> getMyFeeds(){
        return DatabaseHelper.getInstance(getActivity()).getMyItems(mUser.getId());
    }

    private void updateUi() {
        if (feedItemList.size() == 0)
            showView(noItemsFounded);
        else
            hideView(noItemsFounded);
    }

    private void performActionOnListen() {
        feedItemList = getMyFeeds();
            mFeedsAdapter.updateFeedsList(feedItemList);
            updateUi();


    }

}
