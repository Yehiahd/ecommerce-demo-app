package com.fci.yehiahd.ecommerce.controller.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.database.SharedPreferenceHelper;
import com.fci.yehiahd.ecommerce.model.FacebookUser;
import com.fci.yehiahd.ecommerce.util.Connection;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import es.dmoral.toasty.Toasty;

public class BaseActivity extends RxAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected boolean isConnected(){
        return Connection.isNetworkAvailable(this);
    }

    protected boolean isDataValid(EditText... data){
        for (EditText checkET : data) {
            if (TextUtils.isEmpty(checkET.getText().toString().trim())){
                checkET.setError(getString(R.string.error_message));
                checkET.setFocusableInTouchMode(true);
                checkET.requestFocus();
                return false;
            }

            else {
                checkET.setError(null);
            }
        }
        return true;
    }

    protected void clearFields(EditText... data){
        for (EditText editText : data){
            editText.getText().clear();
        }
    }

    protected static void enableDisableRoot(View rootView, boolean enabled) {
        rootView.setEnabled(enabled);
        if ( rootView instanceof ViewGroup) {
            ViewGroup group = (ViewGroup)rootView;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableRoot(group.getChildAt(idx), enabled);
            }
        }
    }

    protected void showView(View view){
        view.setVisibility(View.VISIBLE);
    }

    protected void hideView(View view){
        view.setVisibility(View.GONE);
    }

    protected boolean isSessionActive() {
        return SharedPreferenceHelper.getInstance().getUser(this) != null;
    }

    protected FacebookUser getCurrentUser(){
        return SharedPreferenceHelper.getInstance().getUser(this);
    }

    protected void sucessToasty(String message){
        Toasty.success(this,message, Toast.LENGTH_SHORT).show();
    }

    protected void errorToasty(String message){
        Toasty.error(this,message, Toast.LENGTH_SHORT).show();
    }

    protected void infoToasty(String message){
        Toasty.info(this,message,Toast.LENGTH_SHORT).show();
    }
}
