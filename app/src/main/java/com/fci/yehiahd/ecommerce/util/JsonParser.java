package com.fci.yehiahd.ecommerce.util;

import com.fci.yehiahd.ecommerce.model.FacebookUser;

import org.json.JSONObject;

import rx.Emitter;
import rx.Observable;

/**
 * Created by yehia on 04/03/17.
 */

public class JsonParser {

    public static Observable<FacebookUser> getUserFromJson(JSONObject userData){
        return Observable.fromEmitter(emitter -> {
            try {
                String imgUrl = userData.getJSONObject(Constant.Graph.PICTURE).getJSONObject(Constant.Graph.DATA).getString(Constant.Graph.URL);
                String userName = userData.getString(Constant.Graph.NAME);
                String id = userData.getString(Constant.Graph.ID);

                FacebookUser user = FacebookUser.buildFacebookUser()
                        .setImgUrl(imgUrl)
                        .setUserName(userName)
                        .withId(id)
                        .build();

                emitter.onNext(user);
                emitter.onCompleted();
            }
            catch (Exception e){
                emitter.onError(e);
            }

        }, Emitter.BackpressureMode.NONE);
    }
}
