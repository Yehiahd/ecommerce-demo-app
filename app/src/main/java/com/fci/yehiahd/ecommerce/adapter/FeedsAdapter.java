package com.fci.yehiahd.ecommerce.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fci.yehiahd.ecommerce.R;
import com.fci.yehiahd.ecommerce.callback.OnDeleteClickListener;
import com.fci.yehiahd.ecommerce.callback.OnFavoriteClickListener;
import com.fci.yehiahd.ecommerce.database.DatabaseHelper;
import com.fci.yehiahd.ecommerce.model.FacebookUser;
import com.fci.yehiahd.ecommerce.model.FeedItem;
import com.fci.yehiahd.ecommerce.util.CircleTransform;
import com.fci.yehiahd.ecommerce.util.Constant;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yehia on 04/03/17.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedsHolder> {

    private Context mContext;
    private List<FeedItem> mFeedItemList;
    private FacebookUser mUser;
    private static List<OnFavoriteClickListener> favoriteListeners = new ArrayList<>();
    private static List<OnDeleteClickListener> deleteListeners = new ArrayList<>();
    private boolean isDeleteEnabled;

    public FeedsAdapter(Context context, List<FeedItem> feedsList, FacebookUser user,boolean enableDelete){
        this.mContext = context;
        this.mFeedItemList = feedsList;
        mUser = user;
        this.isDeleteEnabled = enableDelete;
    }

    public void addFavoriteListener(OnFavoriteClickListener listener){
        favoriteListeners.add(listener);
    }
    public void addDeleteListener(OnDeleteClickListener listener){
        deleteListeners.add(listener);
    }

    public void updateFeedsList(List<FeedItem> newFeedsList){
        mFeedItemList.clear();
        mFeedItemList.addAll(newFeedsList);
        this.notifyDataSetChanged();
    }

    @Override
    public FeedsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_list_item,parent,false);
        return new FeedsHolder(view);
    }

    @Override
    public void onBindViewHolder(FeedsHolder holder, int position) {
        FeedItem item = mFeedItemList.get(position);
        populateView(holder,item);
        holder.itemFavorite.setOnClickListener(v -> handleFavoriteProcess(holder,position));
        holder.itemDelete.setOnClickListener(v -> handleDeleteProcess(holder,position));
    }

    @Override
    public int getItemCount() {
        return mFeedItemList.size();
    }

    private void handleFavoriteProcess(FeedsHolder holder, int position) {

        if (mFeedItemList.get(position).isFavorite()){
            DatabaseHelper.getInstance(mContext).setFavorite(mFeedItemList.get(position).getId(), Constant.Database.NOT_FAVORITE);
            mFeedItemList.get(position).setFavorite(Constant.Utils.NOT_FAVORITE);
            holder.itemFavorite.setImageResource(R.drawable.star_off);
        }

        else{
            DatabaseHelper.getInstance(mContext).setFavorite(mFeedItemList.get(position).getId(), Constant.Database.FAVORITE);
            mFeedItemList.get(position).setFavorite(Constant.Utils.FAVORITE);
            holder.itemFavorite.setImageResource(R.drawable.star_on);
        }

        if (favoriteListeners.size() != 0 ){
            for (int i = 0; i < favoriteListeners.size() ; i++){
                if (favoriteListeners.get(i) != null){
                    favoriteListeners.get(i).onFavoriteClicked();
                }
            }
        }

    }

    private void handleDeleteProcess(FeedsHolder holder, int position) {
        new AlertDialog.Builder(mContext)
                .setTitle(Constant.Utils.ALERT_DIALOG_TITLE)
                .setMessage(Constant.Utils.ALERT_DIALOG_MESSAGE)
                .setIcon(R.drawable.ic_swipe)
                .setPositiveButton(Constant.Utils.ALERT_DIALOG_POSITIVE_BTN, (dialog, which) -> {
                    DatabaseHelper.getInstance(mContext).deleteItem(mFeedItemList.get(position).getId());
                    mFeedItemList.remove(position);
                    FeedsAdapter.this.notifyDataSetChanged();
                    notifyDeleteListeners();
                    dialog.dismiss();
                })
                .setNegativeButton(Constant.Utils.ALERT_DIALOG_NEGATIVE_BTN,(dialog, which) -> dialog.dismiss()).show();
    }

    private void notifyDeleteListeners() {
        if (deleteListeners.size() != 0 ){
            for (int i = 0; i < deleteListeners.size() ; i++){
                if (deleteListeners.get(i) != null){
                    deleteListeners.get(i).onDeleted();
                }
            }
        }
    }

    private void populateView(FeedsHolder holder, FeedItem item) {

        if (!isDeleteEnabled){
            holder.itemDelete.setVisibility(View.GONE);
            holder.itemFavorite.setVisibility(View.VISIBLE);
        }

        else {
            holder.itemDelete.setVisibility(View.VISIBLE);
            holder.itemFavorite.setVisibility(View.GONE);
        }

        holder.userName.setText(mUser.getUserName());
        Picasso.with(mContext)
                .load(mUser.getImgUrl())
                .placeholder(R.drawable.progress_placeholder)
                .transform(new CircleTransform())
                .into(holder.userImg);

        Picasso.with(mContext)
                .load(item.getImgUrl())
                .placeholder(R.drawable.progress_placeholder)
                .into(holder.itemImg);
        holder.itemTitle.setText(item.getTitle());
        holder.itemDescription.setText(item.getDescription());
        holder.itemTime.setReferenceTime(Long.parseLong(item.getTimeStamp()));

        if (item.isFavorite())
            holder.itemFavorite.setImageResource(R.drawable.star_on);
        else
            holder.itemFavorite.setImageResource(R.drawable.star_off);

    }

    public class FeedsHolder extends RecyclerView.ViewHolder{

        public FeedsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @BindView(R.id.user_img) ImageView userImg;
        @BindView(R.id.user_name) TextView userName;

        @BindView(R.id.item_img) ImageView itemImg;
        @BindView(R.id.item_title) TextView itemTitle;
        @BindView(R.id.item_description) TextView itemDescription;
        @BindView(R.id.post_time) RelativeTimeTextView itemTime;
        @BindView(R.id.item_favorite) ImageView itemFavorite;
        @BindView(R.id.item_delete) ImageView itemDelete;

    }
}
