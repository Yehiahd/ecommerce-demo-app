package com.fci.yehiahd.ecommerce.model;

/**
 * Created by yehia on 04/03/17.
 */

public class FeedItem {

    private int id;
    private String title;
    private String timeStamp;
    private String description;
    private String imgUrl;
    private boolean isFavorite;

    public FeedItem() {
    }

    public FeedItem(String title, String timeStamp, String description, String imgUrl, boolean isFavorite) {
        this.title = title;
        this.timeStamp = timeStamp;
        this.description = description;
        this.imgUrl = imgUrl;
        this.isFavorite = isFavorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
